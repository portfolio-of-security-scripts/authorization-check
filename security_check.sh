# !/bin/bash
# Author: Your Name
# Title: security_check.sh
# Date: 00/00/202X
# Purpose: Generate a report of all folders that allow Read, Write and Execute for the group "Other".
# Update: Consider adding the server name and logged in users to the 

## Define a variable for date, note you must surround the date command in back tics to capture the command output.  
## If you have a quote it then you will only see the word "date" and not the actual date and time.

# help/usage statement

now=`date`
sclog=security_check.log

if [ "$1" = "-h" ]
  then
    echo "This script will search your local system for any directories that grant \"Other\" full rights."
    echo "usage: $0 [ -h ]"
    exit 0
fi

if [[ "$1" = "-b" && -f $sclog ]]
  then
    cat $sclog >> sc_backup.log
    echo >> sc_backup.log
    echo > $sclog
fi



## Create a header at the top of the security log

echo "*******************************" >> $sclog
echo "**** Start security report ******" >> $sclog

## add hostname to the security log
echo "*** Security report for: $HOSTNAME " >> $sclog

# add date to the security log
echo "*** Report Date: $now " >> $sclog

## Log who was logged into the system when the script was run.
echo "*** Found the following users logged in when this script was run" >> $sclog
who >> $sclog

## Add find command here and redirect the output to $sclog
# Consider that you need find to
# - search for folders and not files
# - search for folder that grant the group "other" read, write and execute "rwx"
# - ignore any content that denies your script access as this meas that "other" has not been granted read, write or execute on the folder folder contents
#echo "You should not be seeing this output.  Remove or comment out this line and add your find command here.  Be sure to redirect your find results to the log file"
find / -type d -perm -007 >> $sclog 2>/dev/null
## add two blank lines
echo >> $sclog
echo >> $sclog
## close log for this run
echo "**** End security report ******" >> $sclog
echo "*******************************" >> $sclog




